import React from "react";
import "./App.css";

function App() {
  const weekdays = [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday",
  ];

  const weekdaysRows = weekdays.map((day, index) => (
    <tr key={index}>
      <td>{day}</td>
    </tr>
  ));

  return (
    <div className="App">
      <h1>W13 weekdays table</h1>
      <table>
        <thead>
          <tr>
            <th>Weekday</th>
          </tr>
        </thead>
        <tbody>{weekdaysRows}</tbody>
      </table>
    </div>
  );
}

export default App;